package airbnb;

import core.BaseTest;
import core.utils.listeners.TestListener;
import io.qameta.allure.*;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.AirBnB.HomePage;
import pages.AirBnB.LoginPage;
import pages.AirBnB.LookingPage;
import pages.AirBnB.ProfilePage;

import java.io.IOException;
import java.lang.reflect.Method;

import static core.utils.extentreports.ExtentTestManager.startTest;

/**
 * Clase de prueba en AirBnB Web
 */

public class TestGoogleWeb extends BaseTest {


    @Test(priority = 1, groups = "generic")
    public void openWebTest(Method method) throws IOException {

        //Open Web App
        getDriver().get("https://www.google.com");

        //Assert
        Assert.assertTrue(getDriver().findElements(By.id("hplogo")).size()>0);

        //Send Keys
        getDriver().findElement(By.xpath("//input[@name=\"q\"]")).sendKeys("Appium");

        //Click Element
        getDriver().findElement(By.xpath("//form[@name=\"gs\"]/div[1]/div[1]/div[2]/ul/li[2]/div[2]/div[1]/span/b")).click();

        String s = "";

    }
}

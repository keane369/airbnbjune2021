package airbnb;

import core.BaseTest;
import core.utils.listeners.AllureListener;
import core.utils.listeners.CustomListener;
import core.utils.listeners.TestListener;
import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pages.AirBnB.HomePage;
import pages.AirBnB.LoginPage;
import pages.AirBnB.LookingPage;
import pages.AirBnB.ProfilePage;
import java.io.IOException;
import java.lang.reflect.Method;

import static core.utils.extentreports.ExtentTestManager.startTest;

/**
 * Clase de prueba en AirBnB
 */
@Listeners(TestListener.class)
public class TestAirBnBApp extends BaseTest {

    /**
     * Tarea
     * 1. Actualizar AirBnB app
     * 2. Actualizar locators
     * 3. Create ProfilePage
     *    - Validate page
     *    - Swipe to Logout
     *    - Validate page
     * 4. Agregar paso 3 en un nuevo Test
     */

    String pathCredentials =  "src/main/resources/airbnb/credentials.json";
    String pathExcel = "src/main/resources/airbnb/destinations.xlsx";

    @Test(priority = 1, groups = "generic")
    @Feature("Open App")
    @Story("Validate open App")
    @Severity(SeverityLevel.BLOCKER)
    @Description("This is a Test to check if application open successfully")
    public void openAppTest(Method method) throws IOException {
        startTest(method.getName(),"This is a Test to check if application open successfully");
        LoginPage login = new LoginPage(getDriver());
        //Assert.assertTrue(login.isLoginPageDisplayed(),"La pagina de Login no esta desplegada");
        //HomePage home = login.login(pathCredentials);
        HomePage home = new HomePage(getDriver());
        Assert.assertTrue(home.isHomePageDisplayed());
        home.takeScreenShot("Home Screen");
    }

    @Test(priority = 2, groups = "playa")
    @Description("This is a Test to enter a beach on the AirBnB flow")
    public void enterBeachTest(Method method){
        startTest(method.getName(),"This is a Test to enter a beach on the AirBnB flow");
        HomePage home = new HomePage(getDriver());
        //1. Enter value on where are you going - Ivan
        home.enterDestination("playa", pathExcel);
        //2. Go through all flow until get price - Gerson
        LookingPage looking = new LookingPage(getDriver());
        looking.getPlace();
        Assert.assertTrue(looking.isResultPageDisplayed());
        looking.clickNavigateBack();
        Assert.assertTrue(home.isHomePageDisplayed());
    }

    @Test(priority = 2, groups = "bosque")
    @Description("This is a Test to enter a forest on the AirBnB flow")
    public void enterForestTest(Method method){
        startTest(method.getName(),"This is a Test to enter a forest on the AirBnB flow");
        HomePage home = new HomePage(getDriver());
        //1. Enter value on where are you going - Ivan
        home.enterDestination("bosque", pathExcel);
        //2. Go through all flow until get price - Gerson
        LookingPage looking = new LookingPage(getDriver());
        looking.getPlace();
        Assert.assertTrue(looking.isResultPageDisplayed());
        looking.clickNavigateBack();
        Assert.assertTrue(home.isHomePageDisplayed());
    }

    @Test(priority = 2, groups = "ciudad")
    @Description("This is a Test to town a beach on the AirBnB flow")
    public void enterTownTest(Method method){
        startTest(method.getName(),"This is a Test to town a beach on the AirBnB flow");
        HomePage home = new HomePage(getDriver());
        //1. Enter value on where are you going - Ivan
        home.enterDestination("ciudad", pathExcel);
        //2. Go through all flow until get price - Gerson
        LookingPage looking = new LookingPage(getDriver());
        looking.getPlace();
        Assert.assertTrue(looking.isResultPageDisplayed());
        looking.clickNavigateBack();
        Assert.assertTrue(home.isHomePageDisplayed());
    }

    @Test(priority = 2, groups = "pais")
    @Description("This is a Test to enter a country on the AirBnB flow")
    public void enterCountryTest(Method method){
        startTest(method.getName(),"This is a Test to enter a country on the AirBnB flow");
        HomePage home = new HomePage(getDriver());
        //1. Enter value on where are you going - Ivan
        home.enterDestination("pais", pathExcel);
        //2. Go through all flow until get price - Gerson
        LookingPage looking = new LookingPage(getDriver());
        looking.getPlace();
        Assert.assertTrue(looking.isResultPageDisplayed());
        looking.clickNavigateBack();
        Assert.assertTrue(home.isHomePageDisplayed());
    }

    @Test(priority = 3, groups = "generic")
    @Description("This is a Test to compare budget")
    public void compareBudgetTest(Method method){
        startTest(method.getName(),"TThis is a Test to compare budget");
        HomePage home = new HomePage(getDriver());
        //3. Save value, compare with budget - Karla
        //4. Print msg and Assert if budget is enough - Ivan
        Assert.assertTrue(home.compareBudget(), "Presupuesto insuficiente :(");
    }

    @Test(priority = 4, groups = "profile")
    @Description("This is a Test to enter to the Terms of service flow")
    public void TermsOfServiceTest(Method method){
        startTest(method.getName(),"This is a Test to enter to the Terms of service flow");
        HomePage home = new HomePage(getDriver());
        ProfilePage profile = home.clickProfileButton();
        Assert.assertTrue(profile.isProfilePageDisplayed());
        Assert.assertTrue((profile.scrollToTermsOfService()));
    }

}

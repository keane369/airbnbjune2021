package pages.AirBnB;

import core.BasePage;
import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class ProfilePage extends BasePage {
    public ProfilePage(AndroidDriver driver) {
        super(driver);
    }

    //Locators
    //Profile element
    @AndroidFindBy(uiAutomator = "new UiSelector().text(\"View profile\")")
    private AndroidElement profileBtn;

    //Terms of service element element
    @AndroidFindBy(uiAutomator = "new UiSelector().text(\"Terms of Service\")")
    private AndroidElement termsOfService;

    //Methods

    /**
     * Check if Profile page is displayed
     * @return
     */
    public boolean isProfilePageDisplayed(){
        System.out.println("Tying to check if Profile page is displayed...");
        return isElementPresent(new MobileBy.ByAndroidUIAutomator("new UiSelector().text(\"View profile\")"));
    }

    /**
     * Swipe to Terms of service method
     * @return - true if element exists
     */
    public boolean scrollToTermsOfService(){
        System.out.println("Trying to swipe to Terms of Service element");
        swipeToElement(new MobileBy.ByAndroidUIAutomator("new UiSelector().text(\"Terms of Service\")"),5);
        return isElementPresent(new MobileBy.ByAndroidUIAutomator("new UiSelector().text(\"Terms of Service\")"));
    }

}

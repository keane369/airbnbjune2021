package pages.AirBnB;

import core.BasePage;
import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.qameta.allure.Step;

import java.io.IOException;

public class HomePage extends BasePage {

    //Constructor
    public HomePage(AndroidDriver driver) {
        super(driver);
    }

    //Locators
    @AndroidFindBy(accessibility="Where are you going? Navigate to start your search.")
    private AndroidElement searchField;

    //Destination Field
    @AndroidFindBy(uiAutomator = "new UiSelector().text(\"Where are you going?Navigate forward to access search suggestions.\")")
    private AndroidElement destinationField;

    //Destination
    @AndroidFindBy(uiAutomator = "new UiSelector().className(\"android.widget.TextView\")")
    private AndroidElement destinationResult;

    //Profile element
    @AndroidFindBy(uiAutomator = "new UiSelector().text(\"Profile\")")
    private AndroidElement profileBtn;

    //Methods
    @Step("Verify Home screen: {0} step...")
    public boolean isHomePageDisplayed(){
        System.out.println("Verificando si la pagina existe...");
        return isElementPresent(new MobileBy.ByAccessibilityId("Where are you going? Navigate to start your search."));
    }

    /**
     * Take screenshot method
     * @param name - File name
     * @throws IOException - Exception
     */
    public void takeSnapshot(String name) throws IOException {
        takeScreenShot(name);
    }

    /**
     * Enter destination method
     * @param destination - value
     */
    public void enterDestination(String destination, String path){
        System.out.println("Trying to enter destination as "+destination+"...");
        searchField.click();
        destinationField.sendKeys(getValueFromExcel(path, destination));
        destinationResult.click();
        System.out.println("Destination entered!");
    }

    /**
     * Click profile button method
     * @return ProfilePage
     */
    public ProfilePage clickProfileButton(){
        System.out.println("Trying to click the Profile button");
        profileBtn.click();
        System.out.println("Profile button clicked!");
        return new ProfilePage(driver);
    }

    public boolean compareBudget(){
        return false;
    }
}

package pages.AirBnB;

import core.BasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import java.util.List;

public class LoginPage extends BasePage {

    //Constructor
    public LoginPage(AndroidDriver driver) {
        super(driver);
    }

    //Locators
    //Continue with emil button
    @AndroidFindBy(uiAutomator = "new UiSelector().text(\"Continue with Email\")")
    private AndroidElement continueWithEmailBtn;

    //Email field
    @AndroidFindBy(uiAutomator = "new UiSelector().text(\"Email\")")
    private AndroidElement emailTxBx;

    //Password field
    @AndroidFindBy(uiAutomator = "new UiSelector().text(\"Password\")")
    private AndroidElement passwordTxBx;

    //Continue button
    @AndroidFindBy(uiAutomator = "new UiSelector().text(\"Continue\")")
    private AndroidElement continueBtn;

    //Methods
    /**
     * Metodo para verificar si la pagina de Login esta despelgada
     * @return true si la pagina esta desplegada
     */
    public boolean isLoginPageDisplayed(){
        System.out.println("Verificando si la pagina existe...");
        return isElementPresent(By.id("2131432301"));
    }

    /**
     * Metodo para dar click al boton Continue with email
     */
    public void clickContinueWithEmail(){
        System.out.println("Trying to click the continue with email button...");
        continueWithEmailBtn.click();
        System.out.println("Continue with email button clicked!");
    }

    /**
     * Metodo para dar click al boton Continue with email
     */
    public void clickContinueBtn(){
        System.out.println("Trying to click the continue button...");
        continueBtn.click();
        System.out.println("Continue button clicked!");
    }

    /**
     * Enter email emthod
     * @param email
     */
    public void enterEmail(String email){
        System.out.println("Trying to enter email value...");
        emailTxBx.sendKeys(email);
        System.out.println("Email value entered");
    }

    /**
     * Enter email emthod
     * @param password
     */
    public void enterPassword(String password){
        System.out.println("Trying to enter password value...");
        passwordTxBx.sendKeys(password);
        System.out.println("Password value entered");
    }

    /**
     * Login method
     */
    public HomePage login(String path){
        clickContinueWithEmail();
        enterEmail(getValueJSON(path,"email"));
        clickContinueBtn();
        enterPassword(getValueJSON(path,"password"));
        clickContinueBtn();
        return new HomePage(driver);
    }



}

package pages.AirBnB;

import core.BasePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

import java.util.List;

public class LookingPage extends BasePage {
    public LookingPage(AndroidDriver driver) {
        super(driver);
    }

    //Locators
    //Find a place to stay
    @AndroidFindBy(uiAutomator = "new UiSelector().className(\"android.widget.TextView\").text(\"Find a place to stay\")")
    private AndroidElement btnFindAPlace;
    //skip calendar
    @AndroidFindBy(uiAutomator = "new UiSelector().className(\"android.widget.Button\").text(\"Skip\")")
    private AndroidElement btnSkip;
    //+ adults
    @AndroidFindBy(uiAutomator = "new UiSelector().description(\"Increment\")")
    private AndroidElement btnIncrement;
    //Search availability
    @AndroidFindBy(uiAutomator = "new UiSelector().text(\"Search\")")
    private AndroidElement btnSearch;

    //Billings header
    @AndroidFindBy(uiAutomator = "new UiSelector().text(\"Billings\")")
    private AndroidElement billingsTx;

    //Navigate back
    @AndroidFindBy(id = "2131430672")
    private AndroidElement navigateBackArrow;

    private void clicBtnFinPlace(){
        System.out.println("Trying to click the find place button");
        btnFindAPlace.click();
        System.out.println("Click to find place button");
    }
    private void clicBtnSkip(){
        System.out.println("Trying to click the skip button");
        btnSkip.click();
        System.out.println("Click to skip button");
    }
    private void incrementResidents(){
        System.out.println("Trying to click increments adults and children");
        List<MobileElement> elements = driver.findElementsByAndroidUIAutomator("new UiSelector().description(\"Increment\")");
        elements.get(0).click();
        System.out.println("Click to increment first adult");
        elements.get(0).click();
        System.out.println("Click to increment second adult");
        elements.get(1).click();
        System.out.println("Click to increment firts children");
    }
    private void clicSearchButton(){
        System.out.println("Trying to click the search button");
        btnSearch.click();
        System.out.println("Click to skip button");
    }
    public void getPlace(){
        clicBtnFinPlace();
        clicBtnSkip();
        incrementResidents();
        clicSearchButton();
    }

    /**
     * Validate if Result page is displayed
     * @return boolean
     */
    public boolean isResultPageDisplayed(){
        System.out.println("Trying to validate if Result page is displayed...");
        return driver.findElementsByAndroidUIAutomator("new UiSelector().textContains(\"places to stay\")").size()>0;
    }

    /**
     * Click back arrow method
     */
    public void clickNavigateBack(){
        System.out.println("Trying to click back arrow...");
        navigateBackArrow.click();
        System.out.println("Back arrow clicked");
    }
}

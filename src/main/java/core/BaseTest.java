package core;
/**
 * Configuracion del driver, Antes y despues de las pruebas
 */

import com.google.common.collect.ImmutableMap;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class BaseTest {

    protected AndroidDriver<AndroidElement> driver;

    @BeforeClass(groups = "generic")
    public void setup() throws MalformedURLException {
        //Capabilities - AirBnB
        DesiredCapabilities capabilities = new DesiredCapabilities();

        //Web Mobile
        /*capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("platformVersion", "11");
        //capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UIAutomator2");
        capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "Chrome");
        capabilities.setCapability("appium:chromeOptions", ImmutableMap.of("w3c", false));
        */

        //Native
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("platformVersion", "11");
        capabilities.setCapability("appActivity", "com.airbnb.android.feat.homescreen.HomeActivity");
        capabilities.setCapability("appPackage", "com.airbnb.android");

        //Capabilities - Notes
        /*capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("platformVersion", "11");
        capabilities.setCapability("appActivity", "com.ogden.memo.ui.MemoMain");
        capabilities.setCapability("appPackage", "com.ogden.memo");
        //capabilities.setCapability("fullReset", "true");
        capabilities.setCapability("noReset", "true");
        capabilities.setCapability("app", "/Users/ivanrivas/Desktop/notes-2-3-5.apk");*/

        //Init driver
        System.out.println("Creando driver...");
        driver = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), capabilities);
        System.out.println("Driver creado!");

        //Wait time
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

    }

    @AfterClass(groups = "generic")
    public void close(){
        System.out.println("Cerrando driver...");
        driver.closeApp();
        System.out.println("Driver cerrado!");
    }

    /**
     * Get Driver method
     */
    public AndroidDriver getDriver(){
        return driver;
    }

    /**
     * Reset data app method
     */
    public void resetData(){
        System.out.println("Trying to reset device data...");
        driver.resetApp();
        System.out.println("Data Reseted!");
    }

    /**
     * Restart app method
     */
    public void restartApp(){
        System.out.println("Trying to remove device data...");
        String packageName = ((AndroidDriver) driver).getCurrentPackage();
        driver.terminateApp(packageName);
        driver.activateApp(packageName);
        System.out.println("Data Removed!");
    }
}

package core;

/**
 * Clase para la configuracion de los Page Object
 */

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.*;
import java.time.Duration;
import java.time.LocalDateTime;

public class BasePage {

    protected AndroidDriver driver;
    private WebDriverWait wait;

    public enum Direction {
        UP,
        DOWN,
        LEFT,
        RIGHT;
    }

    //Constructor
    public BasePage(AndroidDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(60L)), this);
    }

    /**
     * Tap back method
     */
    public void tapBack() {
        System.out.println("Trying to tap back on device...");
        driver.pressKey(new KeyEvent(AndroidKey.BACK));
        System.out.println("Back button tapped.");
    }

    /**
     * Performs swipe from the center of screen
     *
     * @param dir the direction of swipe
     * @version java-client: 7.3.0
     **/
    public void swipeScreen(Direction dir) {
        System.out.println("swipeScreen(): dir: '" + dir + "'"); // always log your actions

        // Animation default time:
        //  - Android: 300 ms
        //  - iOS: 200 ms
        // final value depends on your app and could be greater
        final int ANIMATION_TIME = 200; // ms

        final int PRESS_TIME = 200; // ms

        int edgeBorder = 10; // better avoid edges
        PointOption pointOptionStart, pointOptionEnd;

        // init screen variables
        Dimension dims = driver.manage().window().getSize();

        // init start point = center of screen
        pointOptionStart = PointOption.point(dims.width / 2, dims.height / 2);

        switch (dir) {
            case DOWN: // center of footer
                pointOptionEnd = PointOption.point(dims.width / 2, dims.height - edgeBorder);
                break;
            case UP: // center of header
                pointOptionEnd = PointOption.point(dims.width / 2, edgeBorder);
                break;
            case LEFT: // center of left side
                pointOptionEnd = PointOption.point(edgeBorder, dims.height / 2);
                break;
            case RIGHT: // center of right side
                pointOptionEnd = PointOption.point(dims.width - edgeBorder, dims.height / 2);
                break;
            default:
                throw new IllegalArgumentException("swipeScreen(): dir: '" + dir + "' NOT supported");
        }

        // execute swipe using TouchAction
        try {
            new TouchAction(driver)
                    .press(pointOptionStart)
                    // a bit more reliable when we add small wait
                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(PRESS_TIME)))
                    .moveTo(pointOptionEnd)
                    .release().perform();
        } catch (Exception e) {
            System.err.println("swipeScreen(): TouchAction FAILED\n" + e.getMessage());
            return;
        }

        // always allow swipe action to complete
        try {
            Thread.sleep(ANIMATION_TIME);
        } catch (InterruptedException e) {
            // ignore
        }
    }

    /**
     * Performs swipe from the center of screen n times
     *
     * @param dir the direction of swipe
     * @version java-client: 7.3.0
     **/
    public void swipeScreen(Direction dir, int times) {
        for (int i = 0; i < times; i++) {
            swipeScreen(dir);
        }
    }


    /**
     * Performs swipe inside an element
     *
     * @param el  the element to swipe
     * @param dir the direction of swipe
     * @version java-client: 7.3.0
     **/
    public void swipeElementAndroid(MobileElement el, Direction dir) {
        System.out.println("swipeElementAndroid(): dir: '" + dir + "'"); // always log your actions

        // Animation default time:
        //  - Android: 300 ms
        //  - iOS: 200 ms
        // final value depends on your app and could be greater
        final int ANIMATION_TIME = 200; // ms

        final int PRESS_TIME = 200; // ms

        int edgeBorder;
        PointOption pointOptionStart, pointOptionEnd;

        // init screen variables
        Rectangle rect = el.getRect();
        // sometimes it is needed to configure edgeBorders
        // you can also improve borders to have vertical/horizontal
        // or left/right/up/down border variables
        edgeBorder = 0;

        switch (dir) {
            case DOWN: // from up to down
                pointOptionStart = PointOption.point(rect.x + rect.width / 2,
                        rect.y + edgeBorder);
                pointOptionEnd = PointOption.point(rect.x + rect.width / 2,
                        rect.y + rect.height - edgeBorder);
                break;
            case UP: // from down to up
                pointOptionStart = PointOption.point(rect.x + rect.width / 2,
                        rect.y + rect.height - edgeBorder);
                pointOptionEnd = PointOption.point(rect.x + rect.width / 2,
                        rect.y + edgeBorder);
                break;
            case LEFT: // from right to left
                pointOptionStart = PointOption.point(rect.x + rect.width - edgeBorder,
                        rect.y + rect.height / 2);
                pointOptionEnd = PointOption.point(rect.x + edgeBorder,
                        rect.y + rect.height / 2);
                break;
            case RIGHT: // from left to right
                pointOptionStart = PointOption.point(rect.x + edgeBorder,
                        rect.y + rect.height / 2);
                pointOptionEnd = PointOption.point(rect.x + rect.width - edgeBorder,
                        rect.y + rect.height / 2);
                break;
            default:
                throw new IllegalArgumentException("swipeElementAndroid(): dir: '" + dir + "' NOT supported");
        }

        // execute swipe using TouchAction
        try {
            new TouchAction(driver)
                    .press(pointOptionStart)
                    // a bit more reliable when we add small wait
                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(PRESS_TIME)))
                    .moveTo(pointOptionEnd)
                    .release().perform();
        } catch (Exception e) {
            System.err.println("swipeElementAndroid(): TouchAction FAILED\n" + e.getMessage());
            return;
        }

        // always allow swipe action to complete
        try {
            Thread.sleep(ANIMATION_TIME);
        } catch (InterruptedException e) {
            // ignore
        }
    }

    /**
     * Method to swipes to an element
     * @param locator - Locator to swipe
     * @param swipes - # of swipes
     */
    public void swipeToElement(MobileBy locator, int swipes){
        boolean present = false;
        int count = 0;
        System.out.println("Trying to swipe to element...");
        while (!present || count > swipes){
            present = driver.findElements(locator).size()>0;
            if(present){
                System.out.println("Element found!");
            }else{
                System.out.println("Element not found, swiping...");
                swipeScreen(Direction.UP);
                count++;
            }
        }
        if(count > swipes)
            System.out.println("Element wasn't found after "+swipes+" swipes");
    }


    protected void waitForElementToAppear(By locator) {
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }

    protected void waitForElementToBeVisible(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    protected void waitForElementToDisappear(By locator) {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    protected void waitForTextToDisappear(By locator, String text) {
        wait.until(ExpectedConditions.not(ExpectedConditions.textToBe(locator, text)));
    }

    /**
     * Check if Web element is present
     *
     * @param element
     * @author Ivan Rivas
     */
    protected void waitForElementToAppear(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    /**
     * Get element attribute method
     *
     * @param element   - Web element
     * @param attribute - desired attribute
     * @return attribute value
     */
    protected String getAttributeValue(WebElement element, String attribute) {
        return element.getAttribute(attribute);
    }

    /**
     * Check if Web element is clickeable
     *
     * @param element
     * @author Ivan Rivas
     */
    protected void waitForElementToBeClickable(WebElement element) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    /**
     * Wait for an element to not be present
     *
     * @param element
     * @author Ivan Rivas
     */
    protected void waitForElementToDisappear(WebElement element) {
        wait.until(ExpectedConditions.invisibilityOf(element));
    }

    /**
     * Check method to see if element is present
     *
     * @param locator
     * @return
     */
    protected boolean isElementPresent(By locator) {
        System.out.println("Checking if element is present...");
        if (driver.findElements(locator).size() > 0) {
            System.out.println("Element is present");
            return true;
        } else {
            System.out.println("Element is not present");
            return false;
        }
    }

    /**
     * Check method to see if element is present for mobileBy
     *
     * @param locator
     * @return
     */
    protected boolean isElementPresent(MobileBy locator) {
        System.out.println("Checking if element is present...");
        if (driver.findElements(locator).size() > 0) {
            System.out.println("Element is present");
            return true;
        } else {
            System.out.println("Element is not present");
            return false;
        }
    }

    /**
     * Retreives credentials
     * @return credentials
     */
    public String[] getCredentials(String path){
        JSONParser parser = new JSONParser();
        String [] credentials = new String[2];
        System.out.println("Trying to get credentials...");
        try {
            Object obj = parser.parse(new FileReader(path));

            // A JSON object. Key value pairs are unordered. JSONObject supports java.util.Map interface.
            JSONObject jsonObject = (JSONObject) obj;

            credentials[0] = (String) jsonObject.get("email");
            credentials[1] = (String) jsonObject.get("password");

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Credentials stored!");
        return credentials;
    }

    /**
     * Retreives value selected
     * @return value
     */
    public String getValueJSON(String path, String param){
        JSONParser parser = new JSONParser();
        String value ="";
        System.out.println("Trying to get value of "+param+"...");
        try {
            Object obj = parser.parse(new FileReader(path));

            // A JSON object. Key value pairs are unordered. JSONObject supports java.util.Map interface.
            JSONObject jsonObject = (JSONObject) obj;

            value = (String) jsonObject.get(param);

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Value obtanied: "+ value);
        return value;
    }

    /**
     * Retreives a Value for place to look
     * @return valid url to use
     */
    public String getDestination (String destination){
        JSONParser parser = new JSONParser();
        String url = "";
        try {
            Object obj = parser.parse(new FileReader("src/main/resources/airbnb/destinations.json"));

            // A JSON object. Key value pairs are unordered. JSONObject supports java.util.Map interface.
            JSONObject jsonObject = (JSONObject) obj;

            // A JSON array. JSONObject supports java.util.List interface.
            JSONArray environments = (JSONArray) jsonObject.get(destination);

            //Add values to array
            int position = environments.get(0).toString().indexOf(":");
            url = environments.get(0).toString().substring(position+1).trim();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return url;
    }

    /**
     * Get value from excel method
     * @param filename - File name
     * @param value - Value to look for
     * @return value to use
     */
    public String getValueFromExcel(String filename, String value){
        try{
            //Getting excel file
            FileInputStream file = new FileInputStream(filename);

            //Getting WorkBook
            //Workbook wb = new HSSFWorkbook(file); - old excel format .xls
            Workbook wb = new XSSFWorkbook(file); //.xlsx

            //Getting sheet
            Sheet sheet = wb.getSheet("Sheet1");

            //Getting row
            Row row = sheet.getRow(0);

            //Column
            int colNum = 0;
            String valueFromExcel = null;

            //Getting cell
            for(int i = 0; i < row.getLastCellNum(); i++){
                if(row.getCell(i).getStringCellValue().trim().equalsIgnoreCase(value)){
                    colNum = i;
                    System.out.println("Value found!");
                    break;
                }
            }

            row = sheet.getRow(1);
            valueFromExcel = row.getCell(colNum).getStringCellValue();
            if(!valueFromExcel.isEmpty())
                System.out.println("Value from excel: "+valueFromExcel);
            else
                System.out.println("Value NOT found.");
            return valueFromExcel;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Take screenshot of the page
     * @param name - Name of the file
     * @throws IOException - Exception
     */
    public void takeScreenShot(String name) throws IOException {
        //Screenshots variables
        LocalDateTime date = LocalDateTime.now();
        String path = "src/main/resources/airbnb/ss/";
        name =name+"_"+date+".png";
        System.out.println("Capturing the snapshot of the page...");
        File srcFiler=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(srcFiler, new File(path+name));
        System.out.println("Snapshot saved!");
    }
}